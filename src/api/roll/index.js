import request from '@/common/requests.js'

export const ipQuery = data => {
    let url = `/test/ip`
    return request({ url, method: 'get', data: data })
}

export const pinyinQuery = data => {
    let url = `/test/pinyin`
    return request({ url, method: 'get', data: data })
}

export const phoneQuery = data => {
    let url = `/test/phone`
    return request({ url, method: 'get', data: data })
}

export const videoQuery = data => {
    let url = `/test/video`
    return request({ url, method: 'get', data: data })
}
