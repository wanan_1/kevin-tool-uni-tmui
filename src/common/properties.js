export default {

	// baseURL: 'http://localhost:2526/',
	baseURL:'https://examapi.cpolar.cn/',

	token_name:"accessToken",
	userPhone:"userPhone",
	userID:"userID",
	agree:"agree",
	firstCome:"firstCome",

	location:"location",
	photo:"photo",
	callPhone:"callPhone",

	channelCode:"channelCode",
	city:"city",
	cityCode:"cityCode",
	province:"province",


	//(H5 WEIXIN WEIXINGONGZHONGHAO)
	appType:"",

}
