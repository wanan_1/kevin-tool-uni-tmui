import properties from '@/common/properties.js'

export default function request(options) {
    return new Promise((resolve, reject) => {
        // url
        let url = properties.baseURL + options.url;
        // method
        let method = options.method;
        let form = options.form;
        var accessToken = uni.getStorageSync(properties.token_name);
        // header
        var header = {
            // #ifdef MP-WEIXIN
            platform: 'wxmini',
            // #endif
            // #ifdef APP-PLUS
            platform: 'app',
            // #endif
            // #ifdef H5
            platform: 'h5',
            // #endif
        };

        let routes = getCurrentPages(); // 获取当前打开过的页面路由数组
        let curRoute = routes[routes.length - 1].route // 获取当前页面路由，也就是最后一个打开的页面路由
        if (curRoute) {
            header['pagePath'] = curRoute
        }

        if (accessToken.length > 0) {
            header['Authorization'] = accessToken
        }

        let data = options.data || {};
        console.log('data',data)
        //	请求方式 GET POST
        if (method) {
            method = method.toUpperCase(); //	小写转大写
            if (method == "POST") {
                if (form == "form") {
                    console.log('form == "form"');
                    header['Content-Type'] = 'application/x-www-form-urlencoded'

                } else {
                    console.log('noform == "form"');
                    header['Content-Type'] = 'application/json;charset=UTF-8'
                }

            }

        }


        //	发起请求 加载动画
        if (options.hideLoading == false) {

        } else {
            uni.showLoading({
                title: "加载中"
            })
        }
        console.log('header');
        console.log(header);

        //	发起网络请求
        uni.request({
            header: header,
            url: url,
            method: method || "GET",
            data: data,
            dataType: "json",
            sslVerify: false, //	是否验证ssl证书
            success: res => {
                console.log("拿到的数据");
                console.log(res);
                if (res.statusCode == 401) {
                    // #ifndef MP-WEIXIN
                    uni.navigateTo({
                        url: '/pages/loginPage/loginPage',
                        fail: (res) => {
                            console.log(res);
                        }
                    })
                    // #endif
                    // #ifdef MP-WEIXIN

                    // #endif
                } else if (res.data.code == 200 || res.data.code == 50001) {
                    typeof options.success == "function" && options.success(res.data);
                } else {
                    console.log(res);
                    if (res.data.message) {
                        Promise.resolve().then(() => {
                            uni.showToast({
                                title: res.data.message,
                                duration: 2000,
                                icon: 'none'
                            });
                        })
                    }
                }
                resolve(res.data);
            },
            fail: err => {
                console.log('res.resultCode', JSON.stringify(err));
                uni.showModal({
                    showCancel: false,
                    content: '网络错误，请稍后再试'
                })
                typeof options.fail == "function" && options.fail(err.data);
                reject(err.errMsg);
            },
            complete: (e) => {
                console.log("请求完成");
                uni.hideLoading();
                typeof options.complete == "function" && options.complete(e.data);
                return;
            }
        })
    });

}
