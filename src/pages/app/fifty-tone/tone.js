export const unVoicedHiragana = [
    {
        "text": "あ",
        "tone": "a"
    },
    {
        "text": "い",
        "tone": "i"
    },
    {
        "text": "う",
        "tone": "u"
    },
    {
        "text": "え",
        "tone": "e"
    },
    {
        "text": "お",
        "tone": "o"
    },
    {
        "text": "か",
        "tone": "ka"
    },
    {
        "text": "き",
        "tone": "ki"
    },
    {
        "text": "く",
        "tone": "ku"
    },
    {
        "text": "け",
        "tone": "ke"
    },
    {
        "text": "こ",
        "tone": "ko"
    },
    {
        "text": "さ",
        "tone": "sa"
    },
    {
        "text": "し",
        "tone": "shi"
    },
    {
        "text": "す",
        "tone": "su"
    },
    {
        "text": "せ",
        "tone": "se"
    },
    {
        "text": "そ",
        "tone": "so"
    },
    {
        "text": "た",
        "tone": "ta"
    },
    {
        "text": "ち",
        "tone": "chi"
    },
    {
        "text": "つ",
        "tone": "tsu"
    },
    {
        "text": "て",
        "tone": "te"
    },
    {
        "text": "と",
        "tone": "to"
    },
    {
        "text": "な",
        "tone": "na"
    },
    {
        "text": "に",
        "tone": "ni"
    },
    {
        "text": "ぬ",
        "tone": "nu"
    },
    {
        "text": "ね",
        "tone": "ne"
    },
    {
        "text": "の",
        "tone": "no"
    },
    {
        "text": "は",
        "tone": "ha"
    },
    {
        "text": "ひ",
        "tone": "hi"
    },
    {
        "text": "ふ",
        "tone": "fu"
    },
    {
        "text": "へ",
        "tone": "he"
    },
    {
        "text": "ほ",
        "tone": "ho"
    },
    {
        "text": "ま",
        "tone": "ma"
    },
    {
        "text": "み",
        "tone": "mi"
    },
    {
        "text": "む",
        "tone": "mu"
    },
    {
        "text": "め",
        "tone": "me"
    },
    {
        "text": "も",
        "tone": "mo"
    },
    {
        "text": "や",
        "tone": "ya"
    },
    {
        "text": "",
        "tone": ""
    },
    {
        "text": "ゆ",
        "tone": "yu"
    },
    {
        "text": "",
        "tone": ""
    },
    {
        "text": "よ",
        "tone": "yo"
    },
    {
        "text": "ら",
        "tone": "ra"
    },
    {
        "text": "り",
        "tone": "ri"
    },
    {
        "text": "る",
        "tone": "ru"
    },
    {
        "text": "れ",
        "tone": "re"
    },
    {
        "text": "ろ",
        "tone": "ro"
    },
    {
        "text": "わ",
        "tone": "wa"
    },
    {
        "text": "",
        "tone": ""
    },
    {
        "text": "",
        "tone": ""
    },
    {
        "text": "",
        "tone": ""
    },
    {
        "text": "を",
        "tone": "wo"
    },
    { "text": "ん", "tone": "n" },
]
export const unVoicedKatakana = [
    {
        "text": "ア",
        "tone": "a"
    },
    {
        "text": "イ",
        "tone": "i"
    },
    {
        "text": "ウ",
        "tone": "u"
    },
    {
        "text": "エ",
        "tone": "e"
    },
    {
        "text": "オ",
        "tone": "o"
    },
    {
        "text": "カ",
        "tone": "ka"
    },
    {
        "text": "キ",
        "tone": "ki"
    },
    {
        "text": "ク",
        "tone": "ku"
    },
    {
        "text": "ケ",
        "tone": "ke"
    },
    {
        "text": "コ",
        "tone": "ko"
    },
    {
        "text": "サ",
        "tone": "sa"
    },
    {
        "text": "シ",
        "tone": "shi"
    },
    {
        "text": "ス",
        "tone": "su"
    },
    {
        "text": "セ",
        "tone": "se"
    },
    {
        "text": "ソ",
        "tone": "so"
    },
    {
        "text": "タ",
        "tone": "ta"
    },
    {
        "text": "チ",
        "tone": "chi"
    },
    {
        "text": "ツ",
        "tone": "tsu"
    },
    {
        "text": "テ",
        "tone": "te"
    },
    {
        "text": "ト",
        "tone": "to"
    },
    {
        "text": "ナ",
        "tone": "na"
    },
    {
        "text": "ニ",
        "tone": "ni"
    },
    {
        "text": "ヌ",
        "tone": "nu"
    },
    {
        "text": "ネ",
        "tone": "ne"
    },
    {
        "text": "ノ",
        "tone": "no"
    },
    {
        "text": "ハ",
        "tone": "ha"
    },
    {
        "text": "ヒ",
        "tone": "hi"
    },
    {
        "text": "フ",
        "tone": "fu"
    },
    {
        "text": "ヘ",
        "tone": "he"
    },
    {
        "text": "ホ",
        "tone": "ho"
    },
    {
        "text": "マ",
        "tone": "ma"
    },
    {
        "text": "ミ",
        "tone": "mi"
    },
    {
        "text": "ム",
        "tone": "mu"
    },
    {
        "text": "メ",
        "tone": "me"
    },
    {
        "text": "モ",
        "tone": "mo"
    },
    {
        "text": "ヤ",
        "tone": "ya"
    },
    {
        "text": "",
        "tone": ""
    },
    {
        "text": "ユ",
        "tone": "yu"
    },
    {
        "text": "",
        "tone": ""
    },
    {
        "text": "ヨ",
        "tone": "yo"
    },
    {
        "text": "ラ",
        "tone": "ra"
    },
    {
        "text": "リ",
        "tone": "ri"
    },
    {
        "text": "ル",
        "tone": "ru"
    },
    {
        "text": "レ",
        "tone": "re"
    },
    {
        "text": "ロ",
        "tone": "ro"
    },
    {
        "text": "ワ",
        "tone": "wa"
    },
    {
        "text": "",
        "tone": ""
    },
    {
        "text": "",
        "tone": ""
    },
    {
        "text": "",
        "tone": ""
    },
    {
        "text": "ヲ",
        "tone": "wo"
    },
    { "text": "ン", "tone": "n" },
]

export const voicedHiragana = [
    { "text": "が", "tone": "ga" },
    { "text": "ぎ", "tone": "gi" },
    { "text": "ぐ", "tone": "gu" },
    { "text": "げ", "tone": "ge" },
    { "text": "ご", "tone": "go" },
    { "text": "ざ", "tone": "za" },
    { "text": "じ", "tone": "ji" },
    { "text": "ず", "tone": "zu" },
    { "text": "ぜ", "tone": "ze" },
    { "text": "ぞ", "tone": "zo" },
    { "text": "だ", "tone": "da" },
    { "text": "ぢ", "tone": "ji" },
    { "text": "づ", "tone": "zu" },
    { "text": "で", "tone": "de" },
    { "text": "ど", "tone": "do" },
    { "text": "ば", "tone": "ba" },
    { "text": "び", "tone": "bi" },
    { "text": "ぶ", "tone": "bu" },
    { "text": "べ", "tone": "be" },
    { "text": "ぼ", "tone": "bo" },
    { "text": "ぱ", "tone": "pa" },
    { "text": "ぴ", "tone": "pi" },
    { "text": "ぷ", "tone": "pu" },
    { "text": "ぺ", "tone": "pe" },
    { "text": "ぽ", "tone": "po" },
]

export const voicedKatakana = [
    { "text": "ガ", "tone": "ga" },
    { "text": "ギ", "tone": "gi" },
    { "text": "グ", "tone": "gu" },
    { "text": "ゲ", "tone": "ge" },
    { "text": "ゴ", "tone": "go" },
    { "text": "ザ", "tone": "za" },
    { "text": "ジ", "tone": "ji" },
    { "text": "ズ", "tone": "zu" },
    { "text": "ゼ", "tone": "ze" },
    { "text": "ゾ", "tone": "zo" },
    { "text": "ダ", "tone": "da" },
    { "text": "ヂ", "tone": "ji" },
    { "text": "ヅ", "tone": "zu" },
    { "text": "デ", "tone": "de" },
    { "text": "ド", "tone": "do" },
    { "text": "バ", "tone": "ba" },
    { "text": "ビ", "tone": "bi" },
    { "text": "ブ", "tone": "bu" },
    { "text": "ベ", "tone": "be" },
    { "text": "ボ", "tone": "bo" },
    { "text": "パ", "tone": "pa" },
    { "text": "ピ", "tone": "pi" },
    { "text": "プ", "tone": "pu" },
    { "text": "ペ", "tone": "pe" },
    { "text": "ポ", "tone": "po" },
]

export const contractedHiragana = [
    { "text": "きゃ", "tone": "kya" },
    { "text": "きゅ", "tone": "kyu" },
    { "text": "きょ", "tone": "kyo" },
    { "text": "しゃ", "tone": "sha" },
    { "text": "しゅ", "tone": "shu" },
    { "text": "しょ", "tone": "sho" },
    { "text": "ちゃ", "tone": "cha" },
    { "text": "ちゅ", "tone": "chu" },
    { "text": "ちょ", "tone": "cho" },
    { "text": "にゃ", "tone": "nya" },
    { "text": "にゅ", "tone": "nyu" },
    { "text": "にょ", "tone": "nyo" },
    { "text": "ひゃ", "tone": "hya" },
    { "text": "ひゅ", "tone": "hyu" },
    { "text": "ひょ", "tone": "hyo" },
    { "text": "みゃ", "tone": "mya" },
    { "text": "みゅ", "tone": "myu" },
    { "text": "みょ", "tone": "myo" },
    { "text": "りゃ", "tone": "rya" },
    { "text": "りゅ", "tone": "ryu" },
    { "text": "りょ", "tone": "ryo" },
]

export const contractedKatakana = [
    { "text": "キャ", "tone": "kya" },
    { "text": "キュ", "tone": "kyu" },
    { "text": "キョ", "tone": "kyo" },
    { "text": "シャ", "tone": "sha" },
    { "text": "シュ", "tone": "shu" },
    { "text": "ショ", "tone": "sho" },
    { "text": "チャ", "tone": "cha" },
    { "text": "チュ", "tone": "chu" },
    { "text": "チョ", "tone": "cho" },
    { "text": "ニャ", "tone": "nya" },
    { "text": "ニュ", "tone": "nyu" },
    { "text": "ニョ", "tone": "nyo" },
    { "text": "ヒャ", "tone": "hya" },
    { "text": "ヒュ", "tone": "hyu" },
    { "text": "ヒョ", "tone": "hyo" },
    { "text": "ミャ", "tone": "mya" },
    { "text": "ミュ", "tone": "myu" },
    { "text": "ミョ", "tone": "myo" },
    { "text": "リャ", "tone": "rya" },
    { "text": "リュ", "tone": "ryu" },
    { "text": "リョ", "tone": "ryo" },
]
